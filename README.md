Ellison Magento Project
===

| Name                | Badge |
|:--------------------|:-----:|
| Master Build Status | [![Master Build Status](https://magnum.travis-ci.com/separationdegrees/ellison.svg?token=uQoRSLdtJ4xGJxCSdueo&branch=master)](https://magnum.travis-ci.com/separationdegrees/ellison) |
| Stage Build Status  | [![Stage Build Status](https://magnum.travis-ci.com/separationdegrees/ellison.svg?token=uQoRSLdtJ4xGJxCSdueo&branch=stage)](https://magnum.travis-ci.com/separationdegrees/ellison) |

