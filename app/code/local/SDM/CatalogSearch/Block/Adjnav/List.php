<?php
/**
 * Separation Degrees One
 *
 * Magento catalog search customizations
 *
 * PHP Version 5
 *
 * @category  SDM
 * @package   SDM_CatalogSearch
 * @author    Separation Degrees One <magento@separationdegrees.com>
 * @copyright Copyright (c) 2015 Separation Degrees One (http://www.separationdegrees.com)
 */

/**
 * Layered nav fix when searching
 */
class SDM_CatalogSearch_Block_Adjnav_List extends AdjustWare_Nav_Block_List
{
    /**
     * Layered nav search fix, intentionally empty
     *
     * @return SDM_CatalogSearch_Block_Adjnav_List
     */
    public function setIsSearchMode()
    {
        return $this;
    }
}
