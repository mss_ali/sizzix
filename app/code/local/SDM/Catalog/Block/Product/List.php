<?php
/**
 * Separation Degrees Media
 *
 * Magento catalog customizations
 *
 * PHP Version 5
 *
 * @category  SDM
 * @package   SDM_Catalog
 * @author    Separation Degrees Media <magento@separationdegrees.com>
 * @copyright Copyright (c) 2015 Separation Degrees Media (http://www.separationdegrees.com)
 */

/**
 * SDM_Catalog_Block_Product_List class
 */
class SDM_Catalog_Block_Product_List
    extends AdjustWare_Nav_Block_Rewrite_FrontCatalogProductList
{
    /**
     * Breadcrumbs
     */
    public $_crumbs = null;

    /**
     * Retrieve loaded category collection and add catalog filter type to it
     *
     * @see Mage_Catalog_Block_Product_List::_getProductCollection()
     *
     * @return Mage_Eav_Model_Entity_Collection_Abstract
     */
    protected function _getProductCollection()
    {
        if (is_null($this->_productCollection)) {
            $layer = $this->getLayer();
            /* @var $layer Mage_Catalog_Model_Layer */
            if ($this->getShowRootCategory()) {
                $this->setCategoryId(Mage::app()->getStore()->getRootCategoryId());
            }

            // if this is a product view page
            if (Mage::registry('product')) {
                // get collection of categories this product is associated with
                $categories = Mage::registry('product')->getCategoryCollection()
                    ->setPage(1, 1)
                    ->load();
                // if the product is associated with any category
                if ($categories->count()) {
                    // show products from this category
                    $this->setCategoryId(current($categories->getIterator()));
                }
            }

            $origCategory = null;
            if ($this->getCategoryId()) {
                $category = Mage::getModel('catalog/category')->load($this->getCategoryId());
                if ($category->getId()) {
                    $origCategory = $layer->getCurrentCategory();
                    $layer->setCurrentCategory($category);
                    $this->addModelTags($category);
                }
            }
            $this->_productCollection = $layer->getProductCollection();

            $this->prepareSortableFieldsByCategory($layer->getCurrentCategory());

            if ($origCategory) {
                $layer->setCurrentCategory($origCategory);
            }

            /**
             * Rewrite beings
             */
            $this->_productCollection->addTypeIdFilter()
                ->addDiscountTypeAppliedTags()
                ->addEuroPrices()
                ->removePriceFilter();
            // Mage::log($this->_productCollection->getSelect()->__toString());
            /**
             * Rewrite ends
             */
        }

        return $this->_productCollection;
    }

    /**
     * Get the catalog url without crumbs
     *
     * @return [string
     */
    public function getCrumbFreeUrl()
    {
        if (Mage::getSingleton('sdm_catalogcrumb/crumb')->getFilterCount() < 2) {
            return false;
        }
        return "/" . Mage::getStoreConfig('navigation/general/catalog_category_id');
    }

    /**
     * Returns an array of breadcrumbs from the catalog crumb model
     *
     * @return array
     */
    public function getCrumbTrail()
    {
        return Mage::getSingleton('sdm_catalogcrumb/crumb')
            ->getCrumbTrail();
    }

    /**
     * Gets the current crumb hash based off the page filters
     *
     * @return string
     */
    public function getCrumbHash()
    {
        return Mage::getSingleton('sdm_catalogcrumb/crumb')
            ->getData('hash');
    }

    /**
     * Gets the current crumb hash based off the page filters
     *
     * @return string
     */
    public function getCrumbBaseUrl()
    {
        return Mage::getSingleton('sdm_catalogcrumb/crumb')
            ->getCrumbBaseUrl();
    }

    /**
     * Get's the last crumb from the crumb trail if applicable
     *
     * @return bool|SDM_Taxonomy_Model_Item
     */
    public function getLastCrumb()
    {
        return Mage::getSingleton('sdm_catalogcrumb/crumb')
            ->getLastCrumb();
    }

    /**
     * Get's the last crumb's type from the crumb trail if applicable
     *
     * @return bool|SDM_Taxonomy_Model_Item
     */
    public function getLastCrumbType()
    {
        return Mage::getSingleton('sdm_catalogcrumb/crumb')
            ->getLastCrumbType();
    }

    /**
     * Retrieve Type URL
     *
     * @param string $remove
     *
     * @return string
     */
    public function getCrumbUrl($remove)
    {
        $urlParams = $this->_getUrlParams($remove);

        // Implode all the arrayed filters
        foreach ($urlParams as $filter => $filterValues) {
            if (is_array($filterValues)) {
                $urlParams[$filter] = implode('-', $filterValues);
            }
        }

        $urlParams['crumb'] = $this->getCrumbHash();
        return (
                !isset($urlParams['q'])
                    ? DS . $this->getCrumbBaseUrl()
                    : '/catalogsearch/result/'
            ) . '?' . http_build_query($urlParams);
    }

    /**
     * Add all filters to _query except for the excluded one
     *
     * @param string $remove
     *
     * @return array
     */
    protected function _getUrlParams($remove)
    {
        $remove = explode('|', $remove);
        $removeId = $remove['1'];
        $removeFilter = $remove['0'];
        $urlParams = array();
        foreach ($this->getFilterParams() as $filter => $filterIds) {
            foreach (explode("-", $filterIds) as $filterId) {
                if ($removeFilter != $filter || $removeId != $filterId) {
                    if (!isset($urlParams[$filter])) {
                        $urlParams[$filter] = array();
                    }
                    $urlParams[$filter][] = $filterId;
                }
            }
        }
        // Special case for price
        if ($removeFilter === 'price') {
            unset($urlParams['price']);
        }
        return $urlParams;
    }

    /**
     * Get all the current filter parameters
     * @return array
     */
    public function getFilterParams()
    {
        $adjnavHelper = Mage::helper('adjnav/data');
        Mage::unregister('adjnav_current_session_params');
        return $adjnavHelper->getParams(false);
    }

    /**
     * Get the number of filtered items of a particular product entity type
     *
     * @param  string $type
     * @return int
     */
    public function getCollectionTypeCount($type)
    {
        $type = Mage::helper('sdm_catalog')->getCatalogFilterType($type);

        // Clone the object and reset unneccesary parts for count
        $collection = clone $this->_getProductCollection();
        $collection->addTypeIdFilter($type);
        $collection->removePriceFilter();   // For grouped products by default

        // Preprate the select to count simple/group products
        $select = $collection->getSelect();
        $select->reset(Zend_Db_Select::LIMIT_COUNT);
        $select->reset(Zend_Db_Select::LIMIT_OFFSET);
        $select->reset(Zend_Db_Select::ORDER);
        $select->reset(Zend_Db_Select::COLUMNS);
        $select->reset(Zend_Db_Select::DISTINCT);
        $select->columns('COUNT(DISTINCT(e.entity_id)) AS count');
        // Mage::log($select->__toString());

        $result = $select->query()->fetch();

        return (int)$result['count'];
    }

    /**
     * Gets the current product type filter based off the URL parameter
     *
     * @return string
     */
    public function getCatalogType()
    {
        return Mage::helper('sdm_catalog')->getCatalogType();
    }

    /**
     * Gets the current product filter type name based off URL parameter
     *
     * @param string|null $type
     *
     * @return string
     */
    public function getCatalogTypeName($type = null)
    {
        return Mage::helper('sdm_catalog')->getCatalogTypeName($type);
    }

    /**
     * Checks if a specific type is active, and returns a class name if it is
     *
     * @param  string $type
     * @return string
     */
    public function getActiveClass($type)
    {
        return $this->getCatalogType() == $type ? 'active' : '';
    }

    /**
     * Retrieve Type URL
     *
     * @param  string $type
     * @return string
     */
    public function getTypeUrl($type)
    {
        $urlParams = array();
        $urlParams['_current']      = true;
        $urlParams['_escape']       = true;
        $urlParams['_use_rewrite']  = true;
        $urlParams['_query']        = array('type' => $type);

        // Remove price and special tag filters from projects tab
        if ($type == SDM_Catalog_Helper_Data::IDEA_CODE) {
            $urlParams['_query'] += array('price' => null, 'tag_special' => null);
        }

        // Clean URL by removing uneeded home, no_cache, and p params
        $urlParams['_query'] += array('home' => null, 'no_cache' => null, 'p' => null);

        // Do a custom cleanup of the URL...
        $url = $this->getUrl('*/*/*', $urlParams);
        $urlParts = explode('?', $url);
        if (count($urlParts) === 2) {
            $url  = $this->getUrl();
            if (strpos($urlParts[0], "/catalogsearch/result/index/") === false) {
                $url .= "catalog";
            } else {
                $url .= "catalogsearch/result/index";
            }
            $url .= "?" . $urlParts[1];
        }
        
        return $url;
    }
}
